// contains functions and business logic of our express js app
// all operations it can do will be placed in this file.
// allows us to use the contents in task.js
const Task = require("../models/task.js");

//define the functions to be used in the "taskRoutes.js"

module.exports.getAllTasks = () =>{
	return Task.find({}).then(result=>{
		return result;
	});
};

// function for creating task

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	// waits for the save method to  complete before .then(error / if it saves properly)
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			return task;
		};
	});
};

// function to delete a task

module.exports.deleteTask = (_id) => {
	return Task.findByIdAndRemove(_id).then((result,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			return result;
		};
	});
};

// function to update a task

module.exports.updateTask = (id,update) => {
	return Task.findById(id).then((result,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			result.name = update.name;
			return result.save().then((updatedTask, error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return updatedTask;
				};
			});
		};
	});
};

// ======== ACTIVITY ========

// function to find a specific task

module.exports.searchId = (id) =>{
	return Task.findById(id).then(result=>{
		return result;
	});
};

// function to update a task

module.exports.updateStatus = (id,update) => {
	return Task.findByIdAndUpdate(id).then((result,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			result.status = update.status;
			return result.save().then((updatedStatus, error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return updatedStatus;
				};
			});
		};
	});
};