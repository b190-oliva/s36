// contain all endpoints for our application
// we separate app.js/index.js should be only be concerned with information about the server
// we have to use router method inside express

const express = require("express");
// allows us to access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

//this allows us to use the contents of taskControllers.js in controllers folder
const taskController = require("../controllers/taskControllers.js");

// routes to get all tasks
// get request to be sent at localhost:3000/tasks
router.get("/", (req,res)=> {
	//since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers and send it to the frontend
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/", (req,res)=>{
	taskController.createTask(req.body).then(resultFromController=> res.send(resultFromController));
});

router.delete("/:id", (req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=> res.send(resultFromController));
});

//req.params.id retrieves the id of the document to be updated
//req.body determines what updates needs to be done
router.put("/:id", (req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController));
});

// ============== ACTIVITY ===============

/*
	business logic:
	- Get the task by ID using Mongoose method "findByID"
	- Change the status of the document to complete
	- Save the task
*/

// get route for getting a specific task

router.get("/:id", (req,res) => {
	taskController.searchId(req.params.id).then(resultp => res.send(resultp));
});

// put route to update a certain task

router.put("/:id/complete", (req,res)=>{
	taskController.updateStatus(req.params.id, req.body).then(result=> res.send(result));
});



// modules.exports allows us to export the file where it is inserted to be used by other
module.exports = router;

// mini activity
// create "/" route that will process the request and send the response based on the createTask function inside the taskController.js
