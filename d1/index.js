// basic imports
const express = require("express");
const mongoose = require("mongoose");
// allow us to use contents of "taskRoutes.js" in the routes folder
const taskRoutes = require("./routes/taskRoutes.js");


// Server setup
const app = express();
const port = 3000;


//MongoDB connection

mongoose.connect("mongodb+srv://39dexshaman:Chowking123@wdc028-course-booking.8f4g3cd.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));
// it allows all the task routes created in the taskRoutes.js files to use "/tasks" route
app.use("/tasks", taskRoutes);

// Server running confirmation

app.listen(port, ()=> console.log(`Server running at port ${port}`));
